CC:= gcc

CFLAG := -Wall -Wextra -pedantic -std=c99 -lm -Werror -g

install: HashReader_v1.c
	$(CC) $(CFLAGS) -o Hash HashReader_v1.c

clean:
	@rm -r -v Hash
