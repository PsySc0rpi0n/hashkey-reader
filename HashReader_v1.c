#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "testing.h"

#define HASH_BUFFER 33

int main(int argc, char *argv[]){
    char hashchar, hashkey[HASH_BUFFER] = "";
    FILE *fp = NULL;
    int charcount = 0, hash_count = 0;

    switch(argc){
        case 1: printf("Try './Hash ?' for help on usage!\n");
                return 0;
        case 2: if(!strcmp(argv[1], "?")){
                    printf("Usage: './Hash <path-to-file>'\n");
		    //break;
                }else{
                    if((fp = fopen(argv[1], "r")) == NULL){
                        printf("Error reading file %s!\n", argv[1]);
                        return -1;
                    }
                    while((hashchar = fgetc(fp)) != EOF){
                        if(isalnum(hashchar) && (charcount >= 0 && charcount < 32)){
                                hashkey[charcount++] = hashchar;
                                if(charcount == 32){
									hashkey[strlen(hashkey) + 1] = '\0';
                                    printf("%s\n", hashkey);
                                    hashkey[0] = '\n';
									hash_count++;
									charcount = 0;
                                }
                        }else{
                            charcount = 0;
                            hashkey[0] = '\0';
                        }
                    }
                }
                break;
        default: printf("Too many arguments!\n");
				 return 0;
    }
	Hashs_Found(hash_count);
    return 0;
}
